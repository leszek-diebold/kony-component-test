define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnIncrement **/
    AS_Button_gd475182a2ce4035af34f43c0495f564: function AS_Button_gd475182a2ce4035af34f43c0495f564(eventobject) {
        var self = this;
        return self.increment.call(this);
    }
});