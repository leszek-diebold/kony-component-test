define(function() {

  return {
    value:0,
    constructor: function(baseConfig, layoutConfig, pspConfig) {

    },
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {

    },

    increment:function() {
      value++;
      this.view.lblCount.text = value;
    }
  };
});